import numpy as np
import cv2
from utils import read_file_paths


class KITTISource(object):
    def __init__(self, image_train_dir, label_train_dir, image_test_dir, valid_fraction):
        self.image_train_dir = image_train_dir
        self.label_train_dir = label_train_dir
        self.image_test_dir = image_test_dir
        self.valid_fraction = valid_fraction
        self.image_size = (576, 160)
        self.num_classes = 2
        self.label_colors = {0: np.array([0, 0, 0]),
                             1: np.array([255, 0, 255])}

    def get_train_and_valid_data(self):
        image_paths = read_file_paths(self.image_train_dir)
        label_paths = read_file_paths(self.label_train_dir)
        label_paths = [p for p in label_paths if '_road_' in p]

        num_images = len(image_paths)
        t = int(self.valid_fraction * num_images)
        image_valid_paths = image_paths[:t]
        label_valid_paths = label_paths[:t]
        image_train_paths = image_paths[t:]
        label_train_paths = label_paths[t:]
        return image_train_paths, label_train_paths, image_valid_paths, label_valid_paths

    def get_test_image_paths(self):
        image_paths = read_file_paths(self.image_test_dir)
        return image_paths

    def read_images(self, image_paths):
        images = []
        for path in image_paths:
            image = cv2.imread(path)
            image = cv2.resize(image, self.image_size)
            images.append(image.astype(np.float32))
        return np.asarray(images)

    def read_labels(self, label_paths):
        road_color = np.array([255, 0, 255])
        labels = []
        for path in label_paths:
            image = cv2.imread(path)
            image = cv2.resize(image, self.image_size)

            label = np.all(image == road_color, axis=2)
            # label_bg = np.any(image != road_color, axis=2)
            # label_all = np.dstack([label_bg, label_road])
            label = label.astype(np.int32)
            labels.append(label)
        return np.asarray(labels)
