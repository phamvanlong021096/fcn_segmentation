from utils import read_file_paths
from fcn import FCN_VGG16
from source_kitti import KITTISource


def main():
    image_train_dir = "D:\\data\\data_road\\training\\image_2"
    label_train_dir = "D:\\data\\data_road\\training\\gt_image_2"
    image_test_dir = "D:\\data\\data_road\\testing\\image_2"

    source = KITTISource(image_train_dir, label_train_dir, image_test_dir, valid_fraction=0.1)

    model = FCN_VGG16(num_classes=2)
    model.build()
    model.train(source, num_epochs=10, batch_size=10)

    print("Done!")


if __name__ == '__main__':
    main()
