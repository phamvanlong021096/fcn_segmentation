import os


def read_file_paths(data_dir):
    if not os.path.isdir(data_dir):
        return []

    image_files = [os.path.join(data_dir, f) for f in os.listdir(data_dir)]
    return image_files
