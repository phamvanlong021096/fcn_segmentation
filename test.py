from source_kitti import KITTISource

image_train_dir = "D:\\data\\data_road\\training\\image_2"
label_train_dir = "D:\\data\\data_road\\training\\gt_image_2"
image_test_dir = "D:\\data\\data_road\\testing\\image_2"

source = KITTISource(image_train_dir, label_train_dir, image_test_dir, valid_fraction=0.1)
image_train_paths, label_train_paths, image_valid_paths, label_valid_paths = source.get_train_and_valid_data()
labels = source.read_labels(label_train_paths[:10])

print(labels[0])
