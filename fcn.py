import os

import tensorflow as tf
import numpy as np

from upscale import upsample_layer


class FCN_VGG16(object):
    def __init__(self, num_classes=2):
        self.num_classes = num_classes
        self.learning_rate = 0.0001
        self.vgg16_npy_path = os.getcwd() + '/weights/vgg16/vgg16.npy'
        self.weight_files = os.getcwd() + '/weights/fcn/fcn_vgg16.ckpt'
        self.data_dict = np.load(self.vgg16_npy_path, encoding='latin1').item()

    def build(self):
        print('Building model...')
        self.X = tf.placeholder(tf.float32, shape=[None, None, None, 3], name="input_image")
        self.Y = tf.placeholder(tf.int32, shape=[None, None, None], name="label")
        self.keep_prob = tf.placeholder(tf.float32, name="keep_probability")

        # Build vgg16
        self.conv1_1, self.relu1_1 = self._conv_layer(self.X, 3, 64, 'conv1_1')
        self.conv1_2, self.relu1_2 = self._conv_layer(self.relu1_1, 3, 64, 'conv1_2')
        self.pool1 = self._max_pool(self.relu1_2, 'pool1')

        self.conv2_1, self.relu2_1 = self._conv_layer(self.pool1, 3, 128, 'conv2_1')
        self.conv2_2, self.relu2_2 = self._conv_layer(self.relu2_1, 3, 128, 'conv2_2')
        self.pool2 = self._max_pool(self.relu2_2, 'pool2')

        self.conv3_1, self.relu3_1 = self._conv_layer(self.pool2, 3, 256, 'conv3_1')
        self.conv3_2, self.relu3_2 = self._conv_layer(self.relu3_1, 3, 256, 'conv3_2')
        self.conv3_3, self.relu3_3 = self._conv_layer(self.relu3_2, 3, 256, 'conv3_3')
        self.pool3 = self._max_pool(self.relu3_3, 'pool3')

        self.conv4_1, self.relu4_1 = self._conv_layer(self.pool3, 3, 512, 'conv4_1')
        self.conv4_2, self.relu4_2 = self._conv_layer(self.relu4_1, 3, 512, 'conv4_2')
        self.conv4_3, self.relu4_3 = self._conv_layer(self.relu4_2, 3, 512, 'conv4_3')
        self.pool4 = self._max_pool(self.relu4_3, 'pool4')

        self.conv5_1, self.relu5_1 = self._conv_layer(self.pool4, 3, 512, 'conv5_1')
        self.conv5_2, self.relu5_2 = self._conv_layer(self.relu5_1, 3, 512, 'conv5_2')
        self.conv5_3, self.relu5_3 = self._conv_layer(self.relu5_2, 3, 512, 'conv5_3')
        self.pool5 = self._max_pool(self.relu5_3, 'pool5')

        # Fully conv
        self.conv6, self.relu6 = self._conv_layer(self.pool5, 7, 4096, 'conv6')
        self.dropout6 = tf.nn.dropout(self.relu6, keep_prob=self.keep_prob)
        self.conv7, self.relu7 = self._conv_layer(self.dropout6, 1, 4096, 'conv7')
        self.dropout7 = tf.nn.dropout(self.relu7, keep_prob=self.keep_prob)

        # now to upscale to actual image size
        self.vgg3_reshaped = self._reshape(self.pool3, self.num_classes, 8, 'layer3_resize')
        self.vgg4_reshaped = self._reshape(self.pool4, self.num_classes, 16, 'layer4_resize')
        self.vgg7_reshaped = self._reshape(self.dropout7, self.num_classes, 32, 'layer7_resize')

        self.logits = tf.add(self.vgg3_reshaped, tf.add(2 * self.vgg4_reshaped, 4 * self.vgg7_reshaped))
        self.softmax = tf.nn.softmax(self.logits)  # default axis = -1
        self.classes = tf.argmax(self.softmax, axis=3)

        # get optimizer
        self.loss = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logits, labels=self.Y))
        optimizer = tf.train.AdamOptimizer(self.learning_rate)
        self.opt = optimizer.minimize(self.loss)

        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())

        print('Model initialized.')

    def _conv_layer(self, inputs, filter_size, num_filters, name):
        channels = int(inputs.get_shape()[3])
        if name in self.data_dict:
            w = tf.Variable(self.data_dict[name][0], name=name + '_w')
            b = tf.Variable(self.data_dict[name][1], name=name + '_b')
        else:
            w = self._weight_variable([filter_size, filter_size, channels, num_filters], name=name + '_w')
            b = self._bias_variable([num_filters], name=name + '_b')

        conv = tf.nn.conv2d(inputs, w, [1, 1, 1, 1], padding='SAME')
        conv_bias = tf.nn.bias_add(conv, b)
        conv_relu = tf.nn.relu(conv_bias)
        return conv_bias, conv_relu

    def _max_pool(self, inputs, name):
        return tf.nn.max_pool(inputs, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name=name)

    def _weight_variable(self, shape, name):
        return tf.Variable(tf.truncated_normal(shape, stddev=0.1), name)

    def _bias_variable(self, shape, name):
        return tf.Variable(tf.constant(0.1, shape=shape), name)

    def _reshape(self, inputs, num_classes, upscale_factor, name):
        channels = int(inputs.get_shape()[3])
        w = self._weight_variable([1, 1, channels, num_classes], name=name + '_w')
        b = self._bias_variable([num_classes], name=name + '_b')
        resized = tf.nn.conv2d(inputs, w, [1, 1, 1, 1], padding='VALID')
        resized = tf.nn.bias_add(resized, b)
        upsampled = upsample_layer(resized, num_classes, upscale_factor, name + '_upsampled')
        return upsampled

    def train(self, source, num_epochs, batch_size):
        print("Start training...")
        image_train_paths, label_train_paths, image_valid_paths, label_valid_paths = source.get_train_and_valid_data()
        num_image_train = len(image_train_paths)
        num_image_valid = len(image_valid_paths)
        num_batch_train = (num_image_train - 1) // batch_size + 1
        num_batch_valid = (num_image_valid - 1) // batch_size + 1

        for e in range(num_epochs):
            loss_train = 0
            loss_valid = 0
            # image_train_paths, label_train_paths = shuffle_data()

            # Train
            for i in range(num_batch_train):
                start_index = i * batch_size
                end_index = min(start_index + batch_size, num_image_train)
                batch_X = source.read_images(image_train_paths[start_index: end_index])
                batch_Y = source.read_labels(label_train_paths[start_index: end_index])
                _, loss = self.sess.run([self.opt, self.loss],
                                        feed_dict={self.X: batch_X, self.Y: batch_Y, self.keep_prob: 0.5})
                loss_train += loss
            loss_train /= num_batch_train

            # Valid
            for i in range(num_batch_valid):
                start_index = i * batch_size
                end_index = min(start_index + batch_size, num_image_valid)
                batch_X = source.read_images(image_valid_paths[start_index: end_index])
                batch_Y = source.read_labels(label_valid_paths[start_index: end_index])
                loss = self.sess.run(self.loss,
                                     feed_dict={self.X: batch_X, self.Y: batch_Y, self.keep_prob: 1.0})
                loss_valid += loss
            loss_valid /= num_batch_valid

            print("Step: ", e)
            print("     Loss train: ", loss_train)
            print("     Loss valid: ", loss_valid)

        print("Training done!")
