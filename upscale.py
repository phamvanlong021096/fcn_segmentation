import numpy as np
import tensorflow as tf


def get_bilinear_filter(filter_shape, upscale_factor, name):
    # filter_shape is [width, height, num_in_channels, num_out_channels]
    kernel_size = filter_shape[1]
    # Center location of the filter for which value is calculated
    if kernel_size % 2 == 1:
        centre_location = upscale_factor - 1
    else:
        centre_location = upscale_factor - 0.5

    bilinear = np.zeros([filter_shape[0], filter_shape[1]])
    for x in range(filter_shape[0]):
        for y in range(filter_shape[1]):
            # Interpolation calculation
            value = (1 - abs((x - centre_location) / upscale_factor)) \
                    * (1 - abs((y - centre_location) / upscale_factor))
            bilinear[x, y] = value
    weights = np.zeros(filter_shape)
    for i in range(filter_shape[2]):
        weights[:, :, i, i] = bilinear

    init = tf.constant_initializer(value=weights, dtype=tf.float32)
    bilinear_weights = tf.get_variable(name=name, initializer=init, shape=weights.shape)
    return bilinear_weights


def upsample_layer(x, n_channels, upscale_factor, name):
    kernel_size = 2 * upscale_factor - upscale_factor % 2
    stride = upscale_factor
    strides = [1, stride, stride, 1]
    in_shape = tf.shape(x)

    h = in_shape[1] * stride
    w = in_shape[2] * stride
    new_shape = [in_shape[0], h, w, n_channels]
    output_shape = tf.stack(new_shape)

    filter_shape = [kernel_size, kernel_size, n_channels, n_channels]
    weights = get_bilinear_filter(filter_shape, upscale_factor, name + '_w')
    deconv = tf.nn.conv2d_transpose(x, weights, output_shape, strides=strides, padding='SAME')

    return deconv
